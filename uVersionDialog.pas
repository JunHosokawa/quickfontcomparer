﻿unit uVersionDialog;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts, PK.Utils.WebLabel;

type
  TfrmVersion = class(TForm)
    textTitle: TText;
    textVersion: TText;
    textCopyrightProgram: TText;
    btnVersionClose: TButton;
    textPlatform: TText;
    listTitleUnder: TLine;
    layoutCopyrightBase: TLayout;
    textCopyrighDesign: TText;
    layoutBottom: TLayout;
    imgIcon: TImage;
    imgBuiltWith: TImage;
    procedure imgBuiltWithClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private const
    OVER_COLOR = TAlphaColorRec.Aqua;
    PRESSED_COLOR = TAlphaColorRec.Magenta;
  private var
    FWebLabel: TWebLabel;
  public
    class procedure New(
      const iForm: TCommonCustomForm;
      const Style: TStyleBook);
  end;

implementation

{$R *.fmx}

uses
  PK.Utils.Browser;

procedure TfrmVersion.FormCreate(Sender: TObject);
begin
  FWebLabel :=
    TWebLabel.Create(
      textTitle,
      textTitle.TextSettings.FontColor,
      OVER_COLOR,
      PRESSED_COLOR,
      'https://bitbucket.org/JunHosokawa/quickfontcomparer');
end;

procedure TfrmVersion.FormDestroy(Sender: TObject);
begin
  FWebLabel.DisposeOf;
end;

procedure TfrmVersion.imgBuiltWithClick(Sender: TObject);
begin
  OpenBrowser('https://www.embarcadero.com/jp/products/delphi');
end;

class procedure TfrmVersion.New(
  const iForm: TCommonCustomForm;
  const Style: TStyleBook);
begin
  with TfrmVersion.Create(iForm) do
    try
      StyleBook := Style;
      imgIcon.SendToBack;

      btnVersionClose.SetFocus;
      ShowModal;
    finally
      DisposeOf;
    end;
end;

end.
