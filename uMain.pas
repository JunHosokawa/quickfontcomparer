﻿unit uMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants, System.Generics.Collections,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  FMX.ListBox, FMX.StdCtrls, FMX.Controls.Presentation, FMX.Edit, FMX.Objects,
  FMX.EditBox, FMX.SpinBox, FMX.Colors, FMX.Effects, FMX.Ani, FMX.Menus,
  FMX.Filter.Effects,
  PK.Utils.RectDialog, PK.LimitSize, PK.FontList.Default, PK.FontList.Helper,
  uFavoriteListItem, uColorDialog;

type
  TfrmMain = class(TForm)
    lstbxFontNames: TListBox;
    layoutFontNameBase: TLayout;
    lblFontNameTitle: TLabel;
    layoutFontBase: TLayout;
    splitterFontName: TSplitter;
    layoutFavorites: TLayout;
    lblFavoritesTitle: TLabel;
    lstbxFavorites: TListBox;
    layoutFavoriteTitlesBase: TLayout;
    spinFontSize: TSpinBox;
    styleDark: TStyleBook;
    cmbbxFontStyle: TComboBox;
    edtSample: TEdit;
    colorColorPopup: TColorPanel;
    rectColorPopup: TRectangle;
    btnColorPupupClose: TButton;
    edtColorPopupCode: TEdit;
    btnBackColor: TColorButton;
    btnFontColor: TColorButton;
    rectDisabler: TRectangle;
    indWaiter: TAniIndicator;
    rectWaiter: TRectangle;
    textWaiter: TText;
    rectWaiterCenter: TRectangle;
    btnVersion: TButton;
    rectCopied: TRoundRect;
    animOpacity: TFloatAnimation;
    effectCopied: TGlowEffect;
    barStatus: TStatusBar;
    lblStatusInfo: TLabel;
    menuMain: TMainMenu;
    menuAppMenu: TMenuItem;
    menuAppMenuVersion: TMenuItem;
    layoutFontNamesTopBase: TLayout;
    layoutFontTopOpBase: TLayout;
    edtFontFilter: TEdit;
    chbxFontJapanese: TCheckBox;
    chbxFontMonoSpace: TCheckBox;
    lblFontFilterTitle: TLabel;
    layoutFontFilterBase: TLayout;
    layoutFontFilterTop: TLayout;
    langMain: TLang;
    lblCopied: TLabel;
    btnFontFilterClear: TButton;
    pathFontFilterClear: TPath;
    menuAppSep1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure lstbxFontNamesChangeCheck(Sender: TObject);
    procedure spinFontSizeChange(Sender: TObject);
    procedure edtSampleTyping(Sender: TObject);
    procedure btnFontColorClick(Sender: TObject);
    procedure btnBackColorClick(Sender: TObject);
    procedure lstbxFavoritesApplyStyleLookup(Sender: TObject);
    procedure lstbxFontNamesChange(Sender: TObject);
    procedure cmbbxFontStyleChange(Sender: TObject);
    procedure btnVersionClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure animOpacityFinish(Sender: TObject);
    procedure menuAppMenuQuitClick(Sender: TObject);
    procedure menuAppMenuVersionClick(Sender: TObject);
    procedure menuWindowShowClick(Sender: TObject);
    procedure menuWindowMinimizeClick(Sender: TObject);
    procedure edtFontFilterValidating(Sender: TObject; var Text: string);
    procedure chbxFontMonoSpaceChange(Sender: TObject);
    procedure chbxFontJapaneseChange(Sender: TObject);
    procedure edtFontFilterKeyDown(Sender: TObject; var Key: Word;
      var KeyChar: Char; Shift: TShiftState);
    procedure edtSampleKeyDown(Sender: TObject; var Key: Word;
      var KeyChar: Char; Shift: TShiftState);
    procedure btnFontFilterClearClick(Sender: TObject);
  private const
    PROP_MONOSPACE_JP = '等幅';
    PROP_JAPANESE_JP = '日本語';
    FONTLIST_TITLE = 'F&ont Count: %d';
    FONTLIST_TITLE_JP = 'フォント数: %d';
    DEFAULT_TEXT =
      'The quick brown fox jumps over the lazy dog ' +
      'すばしっこい茶色の狐はのろまな犬を飛び越える';
    ITEM_HEIGHT_K = 3.5;
  private type
    TListUpFavoriteFunc =
      reference to function(const iItem: TFavoriteListItem): Boolean;
  private var
    FCurrent: TFavoriteListItem;
    FColorDialog: TColorDialog;
    FCopiedDialog: TRectDialog;
    FLimitSize: TLimitSize;
    FOldFilterText: String;
    FFontListTitle: String;
    FIsJapanese: Boolean;
  private
    procedure CheckCursorKey(const iKey: Word);
    procedure ListUpFavorite(const iFunc: TListUpFavoriteFunc);
    function FindFavoriteItem(const iItem: TFontPropertyListItem): TFavoriteListItem;
    function AddItem(const iItem: TListBoxItem): TFavoriteListItem;
    procedure SetListBoxFont;
    function GetSampleText: String;
    procedure SetTextSettings(const iItem: TFavoriteListItem);
    procedure SetBackgroundColor;
    procedure LoadFontsToFontNames(
      const iFunc: TLoadFontsDetermineToAddFunc;
      const iProc: TProc);
    procedure Filter(
      const iText: String;
      const iForce: Boolean = False);
    procedure FilterForce;
    procedure ShowVersion;
    procedure FontAdd(Sender: TObject);
    procedure FontDelete(Sender: TObject; const iFontProperty: TFontProperty);
    procedure FontCopied(Sender: TObject);
    procedure FontDetail(
      Sender: TObject;
      const iFontProperty: TFontProperty;
      const iTextSettings: TTextSettings);
    procedure ColorDialogPick(Sender: TObject; const iColor: TAlphaColor);
  end;

var
  frmMain: TfrmMain;

implementation

uses
  PK.FontList
  //, PK.Utils.MacDummyMain
  , PK.Utils.MenuHelper
  , PK.Fix.TaskbarIconClick
  , uDetailDialog
  , uVersionDialog
  {$IFDEF OSX}
  , Macapi.CoreFoundation // H2443 のヒント対策
  {$ENDIF}
  ;

{$R *.fmx}

function TfrmMain.AddItem(const iItem: TListBoxItem): TFavoriteListItem;
begin
  Result :=
    TFavoriteListItem.Create(lstbxFavorites, iItem as TFontPropertyListItem);

  lstbxFavorites.AddObject(Result);

  Result.ApplyStyleLookup;
  Result.OnAdd := FontAdd;
  Result.OnDelete := FontDelete;
  Result.OnCopied := FontCopied;
  Result.OnDetail := FontDetail;

  SetTextSettings(Result);
end;

procedure TfrmMain.animOpacityFinish(Sender: TObject);
begin
  FCopiedDialog.Hide;
end;

procedure TfrmMain.btnBackColorClick(Sender: TObject);
begin
  FColorDialog.Show(btnBackColor);
end;

procedure TfrmMain.btnFontColorClick(Sender: TObject);
begin
  FColorDialog.Show(btnFontColor);
end;

procedure TfrmMain.btnFontFilterClearClick(Sender: TObject);
begin
  edtFontFilter.Text := '';
end;

procedure TfrmMain.btnVersionClick(Sender: TObject);
begin
  ShowVersion;
end;

procedure TfrmMain.chbxFontJapaneseChange(Sender: TObject);
begin
  FilterForce;
end;

procedure TfrmMain.chbxFontMonoSpaceChange(Sender: TObject);
begin
  FilterForce;
end;

procedure TfrmMain.CheckCursorKey(const iKey: Word);
begin
  if (iKey = vkDown) or (iKey = vkUp) then
    lstbxFontNames.SetFocus;
end;

procedure TfrmMain.cmbbxFontStyleChange(Sender: TObject);
begin
  SetListBoxFont;
end;

procedure TfrmMain.ColorDialogPick(Sender: TObject; const iColor: TAlphaColor);
var
  Button: TColorButton;
begin
  Button := FColorDialog.Sender;
  Button.Color := iColor;

  if Button = btnBackColor then
    SetBackgroundColor
  else
    SetListBoxFont;
end;

procedure TfrmMain.edtFontFilterKeyDown(
  Sender: TObject;
  var Key: Word;
  var KeyChar: Char;
  Shift: TShiftState);
begin
  CheckCursorKey(Key);
end;

procedure TfrmMain.edtFontFilterValidating(Sender: TObject; var Text: string);
begin
  Filter(Text);
end;

procedure TfrmMain.edtSampleKeyDown(
  Sender: TObject;
  var Key: Word;
  var KeyChar: Char;
  Shift: TShiftState);
begin
  CheckCursorKey(Key);
end;

procedure TfrmMain.edtSampleTyping(Sender: TObject);
begin
  SetListBoxFont;
end;

procedure TfrmMain.Filter(const iText: String; const iForce: Boolean = False);
var
  IsMono: Boolean;
  IsJapanese: Boolean;
  UpperText: String;
begin
  IsMono := chbxFontMonoSpace.IsChecked;
  IsJapanese := chbxFontJapanese.IsChecked;

  UpperText := iText.ToUpper;

  if (FOldFilterText = UpperText) and (not iForce) then
    Exit;

  FOldFilterText := UpperText;

  LoadFontsToFontNames(
    function(const iFontProperty: TFontProperty): Boolean
    begin
      Result :=
        (FOldFilterText.IsEmpty) or
        (iFontProperty.DisplayName.ToUpper.IndexOf(FOldFilterText) > -1);

      if IsMono and not iFontProperty.IsMonoSpace then
        Result := False;

      if IsJapanese and not iFontProperty.IsJapanese then
        Result := False;
    end,

    procedure
    var
      Item: TFmxObject;
      FPItem: TFontPropertyListItem absolute Item;
      i: Integer;
      FavItem: TFavoriteListItem;
    begin
      for i := 0 to lstbxFontNames.Content.ChildrenCount - 1 do
      begin
        Item := lstbxFontNames.Content.Children[i];

        if not (Item is TFontPropertyListItem) then
          Continue;

        FavItem := FindFavoriteItem(FPItem);

        if FavItem <> nil then
        begin
          FavItem.SetFontPropertyFromItem(FPItem);
          FPItem.IsChecked := True;
        end;
      end;
    end
  );
end;

procedure TfrmMain.FilterForce;
begin
  Filter(edtFontFilter.Text, True);
end;

function TfrmMain.FindFavoriteItem(
  const iItem: TFontPropertyListItem): TFavoriteListItem;
var
  ResultItem: TFavoriteListItem;
begin
  ResultItem := nil;

  ListUpFavorite(
    function (const iFavItem: TFavoriteListItem): Boolean
    begin
      Result := True;

      if iFavItem.FontProperty.IsEqual(iItem.FontProperty) then
      begin
        Result := False;
        ResultItem := iFavItem;
      end;
    end
  );

  Result := ResultItem;
end;

procedure TfrmMain.FontAdd(Sender: TObject);
var
  Item: TListBoxItem;
  FPItem: TFontPropertyListItem absolute Item;
begin
  Item := lstbxFontNames.Selected;
  if (Item = nil) or not (Item is TFontPropertyListItem) then
    Exit;

  Item.IsChecked := True;

  if FindFavoriteItem(FPItem) = nil then
    AddItem(Item);
end;

procedure TfrmMain.FontCopied(Sender: TObject);
begin
  FCopiedDialog.Show;
  animOpacity.Start;
end;

procedure TfrmMain.FontDelete(
  Sender: TObject;
  const iFontProperty: TFontProperty);
var
  i: Integer;
  Obj: TFmxObject;
  Item: TFontPropertyListItem absolute Obj;
begin
  for i := 0 to lstbxFontNames.Content.ChildrenCount - 1 do
  begin
    Obj := lstbxFontNames.Content.Children[i];
    if
      (Obj is TFontPropertyListItem) and
      iFontProperty.IsEqual(Item.FontProperty)
    then
    begin
      Item.IsChecked := False;
      Break;
    end;
  end;
end;

procedure TfrmMain.FontDetail(
  Sender: TObject;
  const iFontProperty: TFontProperty;
  const iTextSettings: TTextSettings);
begin
  TfrmDetailDialog.New(
    Self,
    GetSampleText,
    iFontProperty,
    iTextSettings,
    btnBackColor.Color);
end;

procedure TfrmMain.FormCreate(Sender: TObject);

  procedure SetControlWidth(const iControl: TPresentedTextControl);
  begin
    iControl.Width := iControl.Canvas.TextWidth(iControl.Text) + 32;
  end;

begin
  FLimitSize := TLimitSIze.Create(Self);
  FLimitSize.Parent := Self;
  FLimitSize.SetLimit(800, 400, 0, 0);

  {$IFDEF MSWINDOWS}
  menuMain.DisposeOf;
  {$ENDIF}

  {$IFDEF OSX}
  menuMain.ChangeLang(langMain);
  {$ENDIF}

  FColorDialog :=
    TColorDialog.Create(
      rectColorPopup,
      rectDisabler,
      colorColorPopup,
      edtColorPopupCode,
      btnColorPupupClose);
  FColorDialog.OnPick := ColorDialogPick;

  FCopiedDialog := TRectDialog.Create(rectCopied, nil);

  FCurrent := AddItem(nil);

  SetListBoxFont;

  FColorDialog.Hide;
  FCopiedDialog.Hide;

  // Catalina からメニュー構成が変わったためウィンドウにに新規ウィンドウメニュー
  // を付けないといけなくなったため取りあえず、macOS 風の動作になる処理を削除
  //ReplaceMainFormForMac;

  FIsJapanese := langMain.Lang = 'ja';

  FFontListTitle := FONTLIST_TITLE;
  if FIsJapanese then
  begin
    SetControlWidth(chbxFontJapanese);
    SetControlWidth(chbxFontMonospace);
    SetControlWidth(btnVersion);

    FFontListTitle := FONTLIST_TITLE_JP;
  end;

  TThread.ForceQueue(
    TThread.Current,
    procedure
    begin
      LoadFontsToFontNames(
        nil,
        procedure
        begin
          lstbxFontNames.SetFocus;
        end
      );
    end
  );
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  FLimitSize.DisposeOf;
  FColorDialog.DisposeOf;
  FCopiedDialog.DisposeOf;
end;

function TfrmMain.GetSampleText: String;
begin
  Result := edtSample.Text;
  if Result.IsEmpty then
    Result := DEFAULT_TEXT;
end;

procedure TfrmMain.ListUpFavorite(const iFunc: TListUpFavoriteFunc);
var
  i: Integer;
  Obj: TFmxObject;
  Item: TFavoriteListItem absolute Obj;
begin
  for i := 1 to lstbxFavorites.Content.ChildrenCount - 1 do
  begin
    Obj := lstbxFavorites.Content.Children[i];
    if not (Obj is TFavoriteListItem) or (Item = FCurrent) then
      Continue;

    if not iFunc(Item) then
      Break;
  end;
end;

procedure TfrmMain.LoadFontsToFontNames(
  const iFunc: TLoadFontsDetermineToAddFunc;
  const iProc: TProc);
begin
  lblStatusInfo.Text := '';
  indWaiter.Enabled := True;
  rectWaiter.Visible := True;

  lstbxFontNames.Clear;

  LoadFonts(
    lstbxFontNames,
    iFunc,
    procedure
    var
      Obj: TFmxObject;
      Item: TFontPropertyListItem absolute Obj;
    begin
      if FIsJapanese and (lstbxFontNames.Content.ChildrenCount > 0) then
        for Obj in lstbxFontNames.Content.Children do
        begin
          if Obj is TFontPropertyListItem then
          begin
            Item.FontProperty.JapaneseStr := PROP_JAPANESE_JP;
            Item.FontProperty.MonospaceStr := PROP_MONOSPACE_JP;
          end;
        end;

      indWaiter.Enabled := False;
      rectWaiter.Visible := False;

      lblFontNameTitle.Text :=
        Format(FFontListTitle, [lstbxFontNames.Items.Count]);

      if Assigned(iProc) then
        iProc;
    end
  );
end;

procedure TfrmMain.lstbxFavoritesApplyStyleLookup(Sender: TObject);
begin
  SetBackgroundColor;
end;

procedure TfrmMain.lstbxFontNamesChange(Sender: TObject);
var
  Item: TListBoxItem;
  FPItem: TFontPropertyListItem absolute Item;
begin
  Item := lstbxFontNames.Selected;
  if (Item = nil) or not (Item is TFontPropertyListItem) then
    Exit;

  FCurrent.SetFontPropertyFromItem(FPItem);

  SetTextSettings(FCurrent);

  lblStatusInfo.Text := FPItem.FontProperty.ToString;
end;

procedure TfrmMain.lstbxFontNamesChangeCheck(Sender: TObject);
var
  Item: TFontPropertyListItem absolute Sender;
begin
  if not (Sender is TFontPropertyListItem) then
    Exit;

  if Item.IsChecked then
    AddItem(Item)
  else
    ListUpFavorite(
      function (const iFavItem: TFavoriteListItem): Boolean
      begin
        Result := True;
        if iFavItem.FontProperty.RealName = Item.FontProperty.RealName then
        begin
          lstbxFavorites.Items.Delete(iFavItem.Index);
          Result := False;
        end;
      end
    );
end;

procedure TfrmMain.menuAppMenuQuitClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmMain.menuAppMenuVersionClick(Sender: TObject);
begin
  ShowVersion;
end;

procedure TfrmMain.menuWindowMinimizeClick(Sender: TObject);
begin
  WindowState := TWindowState.wsMinimized;
end;

procedure TfrmMain.menuWindowShowClick(Sender: TObject);
begin
  Show;
end;

procedure TfrmMain.SetBackgroundColor;
var
  Back: TRectangle;
begin
  if lstbxFavorites.FindStyleResource<TRectangle>('background', Back) then
    Back.Fill.Color := btnBackColor.Color;
end;

procedure TfrmMain.SetListBoxFont;
var
  Height: Single;
begin
  SetTextSettings(FCurrent);

  Height := spinFontSize.Value;
  if Height < 16 then
    Height := 16;

  lstbxFavorites.ItemHeight := Height * ITEM_HEIGHT_K;

  lstbxFavorites.BeginUpdate;
  try
    ListUpFavorite(
      function (const iFavItem: TFavoriteListItem): Boolean
      begin
        Result := True;
        SetTextSettings(iFavItem);
      end
    );
  finally
    lstbxFavorites.EndUpdate;
  end;

  lstbxFavorites.InvalidateContentSize;
end;

procedure TfrmMain.SetTextSettings(const iItem: TFavoriteListItem);
var
  Style: TFontStyles;
  Text: String;
begin
  Style := [];
  case cmbbxFontStyle.ItemIndex of
    1: // Bold
      Style := [TFontStyle.fsBold];

    2: // Italic
      Style := [TFontStyle.fsItalic];

    3: // Bold + Italic
      Style := [TFontStyle.fsBold, TFontStyle.fsItalic];
  end;

  iItem.SetFontPop(spinFontSize.Value, btnFontColor.Color, Style);

  Text := GetSampleText;

  if (iItem <> nil) then
    Text := iItem.PadSpaces + Text;

  iItem.Text := Text;
end;

procedure TfrmMain.ShowVersion;
begin
  TfrmVersion.New(Self, StyleBook);
end;

procedure TfrmMain.spinFontSizeChange(Sender: TObject);
begin
  SetListBoxFont;
end;

end.
