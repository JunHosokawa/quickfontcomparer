program QuickFontComparer;

uses
  System.StartUpCopy,
  FMX.Forms,
  uMain in 'uMain.pas' {frmMain},
  uFavoriteListItem in 'uFavoriteListItem.pas',
  uColorDialog in 'uColorDialog.pas',
  PK.FontList in 'Lib\FontList\PK.FontList.pas',
  PK.FontList.Default in 'Lib\FontList\PK.FontList.Default.pas',
  PK.FontList.Win in 'Lib\FontList\PK.FontList.Win.pas',
  PK.FontList.Mac in 'Lib\FontList\PK.FontList.Mac.pas',
  PK.FontList.Helper in 'Lib\FontList\PK.FontList.Helper.pas',
  PK.LimitSize.Mac in 'Lib\LimitSize\PK.LimitSize.Mac.pas',
  PK.LimitSize in 'Lib\LimitSize\PK.LimitSize.pas',
  PK.LimitSize.Win in 'Lib\LimitSize\PK.LimitSize.Win.pas',
  PK.Utils.RectDialog in 'Lib\Utils\PK.Utils.RectDialog.pas',
  PK.Utils.MacDummyMain in 'Lib\Utils\PK.Utils.MacDummyMain.pas',
  uVersionDialog in 'uVersionDialog.pas' {frmVersion},
  PK.Utils.Browser in 'Lib\Utils\PK.Utils.Browser.pas',
  PK.Utils.MenuHelper in 'Lib\Utils\PK.Utils.MenuHelper.pas',
  uDetailDialog in 'uDetailDialog.pas' {frmDetailDialog},
  PK.Fix.TaskbarIconClick in 'Lib\Fix\PK.Fix.TaskbarIconClick.pas',
  PK.Utils.WebLabel in 'Lib\Utils\PK.Utils.WebLabel.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
