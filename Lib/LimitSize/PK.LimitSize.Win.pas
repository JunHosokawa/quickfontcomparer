﻿(*
 * Window size limiter
 *
 * PLATFORMS
 *   Windows / macOS
 *
 * LICENSE
 *   Copyright (c) 2017, 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * HOW TO USE
 *   uses PK.LimitSize;
 *
 *   type
 *     TForm1 = class(TForm)
 *       procedure FormCreate(Sender: TObject);
 *     private
 *       FLimitSize: TLimitSize;
 *     end;
 *
 *   procedure TForm1.FormCreate(Sender: TObject);
 *   begin
 *     FLimitSize := TLimitSize.Create(Self);
 *     FLimitSize.SetLimit(180, 160, 1024, 768);
 *   end;
 *
 * 2017/11/17 Version 1.0.3
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

unit PK.LimitSize.Win;

{$IFNDEF MSWINDOWS}
{$WARNINGS OFF 1011}
interface
implementation
end.
{$ENDIF}

interface

implementation

uses
  PK.LimitSize
  , System.Classes, System.SysUtils, System.Generics.Collections
  , Winapi.Windows, Winapi.Messages
  , FMX.Types, FMX.Controls, FMX.Platform, FMX.Platform.Win
  ;

type
  TLimitSizeWin = class(TInterfacedObject, ILimitSize)
  private type
    TLimitSizeWins = TDictionary<HWND, TLimitSizeWin>;
  private class var
    SLimitSizeWins: TLimitSizeWins;
  private var
    FWnd: HWND;
    FWndProc: Pointer;
    FMinW, FMinH: Integer;
    FMaxW, FMaxH: Integer;
  public
    constructor Create; reintroduce;
    destructor Destroy; override;
    function SetLimit(
      const AHandle: TWindowHandle;
      const AMinWidth, AMinHeight, AMaxWidth, AMaxHeight: Integer): Boolean;
  end;

  TLimitSizeFactoryWin = class(TLimitSizeFactory)
  public
    function CreateLimitSize: ILimitSize; override;
  end;

procedure RegisterLimitSizeWin;
begin
  TPlatformServices.Current.AddPlatformService(
    ILimitSizeFactory,
    TLimitSizeFactoryWin.Create);
end;

function WndProc(
  hWnd: HWND;
  uMsg: DWORD;
  wParam: WPARAM;
  lParam: LPARAM): LRESULT; stdcall;
var
  LimitSizeWin: TLimitSizeWin;
begin
  LimitSizeWin := nil;

  if (TLimitSizeWin.SLimitSizeWins.ContainsKey(hWnd)) then
  begin
    LimitSizeWin := TLimitSizeWin.SLimitSizeWins[hWnd];

    case uMsg of
      WM_GETMINMAXINFO:
      begin
        with PMinMaxInfo(lParam)^ do
        begin
          if (LimitSizeWin.FMinW > 0) then
            ptMinTrackSize.X := LimitSizeWin.FMinW;

          if (LimitSizeWin.FMinH > 0) then
            ptMinTrackSize.Y := LimitSizeWin.FMinH;

          if (LimitSizeWin.FMaxW > 0) then
            ptMaxTrackSize.X := LimitSizeWin.FMaxW;

          if (LimitSizeWin.FMaxH > 0) then
            ptMaxTrackSize.Y := LimitSizeWin.FMaxH;
        end;
      end;
    end;
  end;

  if (LimitSizeWin = nil) then
    Result := DefWindowProc(hWnd, uMsg, wParam, lParam)
  else
    Result := CallWindowProc(LimitSizeWin.FWndProc, hWnd, uMsg, wParam, lParam);
end;

{ TLimitSizeWin }

constructor TLimitSizeWin.Create;
begin
  inherited;

  if (SLimitSizeWins = nil) then
    SLimitSizeWins := TLimitSizeWins.Create;
end;

destructor TLimitSizeWin.Destroy;
begin
  if (SLimitSizeWins.ContainsKey(FWnd)) then
  begin
    SetWindowLong(FWnd, GWL_WNDPROC, Integer(FWndProc));
    SLimitSizeWins.Remove(FWnd);
  end;

  inherited;
end;

function TLimitSizeWin.SetLimit(
  const AHandle: TWindowHandle;
  const AMinWidth, AMinHeight, AMaxWidth, AMaxHeight: Integer): Boolean;
var
  Wnd: HWND;
begin
  Result := False;

  FMinW := AMinWidth;
  FMinH := AMinHeight;
  FMaxW := AMaxWidth;
  FMaxH := AMaxHeight;

  Wnd := WindowHandleToPlatform(AHandle).Wnd;
  if (Wnd = 0) or (SLimitSizeWins.ContainsKey(Wnd)) then
    Exit;

  FWnd := Wnd;

  FWndProc := Pointer(SetWindowLong(Wnd, GWL_WNDPROC, Integer(@WndProc)));
  if (FWndProc = nil) then
    Exit;

  SLimitSizeWins.Add(FWnd, Self);

  Result := True;
end;

{ TLimitSizeFactoryWin }

function TLimitSizeFactoryWin.CreateLimitSize: ILimitSize;
begin
  Result := TLimitSizeWin.Create;
end;

initialization
  RegisterLimitSizeWin;

finalization
  TLimitSizeWin.SLimitSizeWins.DisposeOf;

end.
