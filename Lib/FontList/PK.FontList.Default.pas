﻿(*
 * Font List Getter
 *
 * PLATFORMS
 *   Windows / macOS
 *
 * LICENSE
 *   Copyright (c) 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * HOW TO USE
 *   uses PK.FontList, PK.FontList.Helper;
 *
 *   type
 *     TForm1 = class(TForm)
 *       ListBox1: TListBox;
 *       procedure FormCreate(Sender: TObject);
 *     private
 *     end;
 *
 *   procedure TForm1.FormCreate(Sender: TObject);
 *   begin
 *     LoadFonts(ListBox1, nil, nil);
 *   end;
 *
 * 2018/04/08 Version 1.0.0
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

unit PK.FontList.Default;

interface

uses
  System.Classes
  , System.Generics.Collections
  ;

type
  TFontProperty = class
  private const
    STR_MONOSPACE = 'Monospace';
    STR_JAPANESE = 'Japanese available';
    FORMAT_FONT_NAME = '%s / %s';
    FORMAT_PROP = '  ( %s )';
  private var
    FDisplayName: String;
    FRealName: String;
    FIsMonoSpace: Boolean;
    FIsJapanese: Boolean;
    FMonospaceStr: String;
    FJapaneseStr: String;
  private
    function GetDisplayIsReal: Boolean;
  public class
    function IsEqual(const iProp1, iProp2: TFontProperty): Boolean; overload;
  public
    constructor Create(
      const iDisplayName, iRealName: String;
      const iIsMonoSpace, iIsJapan: Boolean;
      const iMonospaceStr: String = STR_MONOSPACE;
      const iJapaneseStr: String = STR_JAPANESE); reintroduce;
    function Clone: TFontProperty;
    function IsEqual(const iTarget: TFontProperty): Boolean; overload;
    function ToString: String; override;
    property DisplayName: String read FDisplayName;
    property RealName: String read FRealName;
    property IsMonoSpace: Boolean read FIsMonoSpace;
    property IsJapanese: Boolean read FIsJapanese;
    property JapaneseStr: String read FJapaneseStr write FJapaneseStr;
    property MonospaceStr: String read FMonospaceStr write FMonospaceStr;
    property DisplayIsReal: Boolean read GetDisplayIsReal;
  end;

  TFontPropertyList = class
  private var
    FList: TList<TFontProperty>;
    function GetCount: Integer;
    function GetItem(const iIndex: Integer): TFontProperty;
    procedure SetItem(const iIndex: Integer; const Value: TFontProperty);
  public
    constructor Create; reintroduce;
    destructor Destroy; override;
    procedure Add(const iItem: TFontProperty);
    procedure Delete(const iIndex: Integer);
    procedure Clear;
    procedure Sort;
    function IndexOf(const iRealName: String): Integer;
    property Count: Integer read GetCount;
    property Items[const iIndex: Integer]: TFontProperty
      read GetItem
      write SetItem; default;
  end;

  TFontPropertyListEnumerator = class(TObject)
  private
    FList: TFontPropertyList;
    FIndex: Integer;
  public
    constructor Create(const iList: TFontPropertyList); reintroduce;
    function GetCurrent: TFontProperty;
    function MoveNext: Boolean;
    procedure Reset;
    property Current: TFontProperty read GetCurrent;
  end;

  IFontList = interface
    ['{C0AC129E-AE58-4819-A74E-9D058BB9858C}']
    procedure GetFontList(const iList: TFontPropertyList);
  end;

  IFontListFactory = interface
    ['{5D182DC9-74C3-4E4D-9CAE-EEDF81B1C6E5}']
    function CreateFontList: IFontList;
  end;

  TFontListFactory = class(TInterfacedObject, IFontListFactory)
  public
    function CreateFontList: IFontList; virtual; abstract;
  end;

implementation

uses
  System.SysUtils
  , System.Generics.Defaults
  ;

{ TFontProperty }

function TFontProperty.Clone: TFontProperty;
begin
  Result :=
    TFontProperty.Create(
      FDisplayName,
      FRealName,
      FIsMonoSpace,
      FIsJapanese,
      FMonospaceStr,
      FJapaneseStr
    );
end;

constructor TFontProperty.Create(
  const iDisplayName, iRealName: String;
  const iIsMonoSpace, iIsJapan: Boolean;
  const iMonospaceStr: String = STR_MONOSPACE;
  const iJapaneseStr: String = STR_JAPANESE); 
begin
  inherited Create;

  FDisplayName := iDisplayName;
  FRealName := iRealName;
  FIsMonoSpace := iIsMonoSpace;
  FIsJapanese := iIsJapan;
  FMonospaceStr := iMonospaceStr;
  FJapaneseStr := iJapaneseStr;
end;

function TFontProperty.GetDisplayIsReal: Boolean;
begin
  Result := FDisplayName = FRealName;
end;

class function TFontProperty.IsEqual(
  const iProp1, iProp2: TFontProperty): Boolean;
begin
  if (iProp1 = nil) or (iProp2 = nil) then
    Exit(False);

  Result := iProp1.FRealName = iProp2.FRealName;
end;

function TFontProperty.IsEqual(const iTarget: TFontProperty): Boolean;
begin
  Result := IsEqual(Self, iTarget);
end;

function TFontProperty.ToString: String;
var
  SL: TStringList;
begin
  if DisplayIsReal then
    Result := FDisplayName
  else
    Result := Format(FORMAT_FONT_NAME, [FDisplayName, FRealName]);

  SL := TStringList.Create;
  try
    if FIsMonoSpace then
      SL.Add(FMonospaceStr);

    if FIsJapanese then
      SL.Add(FJapaneseStr);

    if SL.Count > 0 then
      Result :=
        Result +
        Format(FORMAT_PROP, [SL.CommaText.Replace(',', ', ').Replace('"', '')]);
  finally
    SL.DisposeOf;
  end;
end;

{ TFontPropertyListEnumerator }

constructor TFontPropertyListEnumerator.Create(const iList: TFontPropertyList);
begin
  inherited Create;

  FList := iList;
  Reset;
end;

function TFontPropertyListEnumerator.GetCurrent: TFontProperty;
begin
  Result := FList[FIndex];
end;

function TFontPropertyListEnumerator.MoveNext: Boolean;
begin
  Inc(FIndex);
  Result := (FIndex < FList.Count);
end;

procedure TFontPropertyListEnumerator.Reset;
begin
  FIndex := -1;
end;

{ TFontPropertyList }

procedure TFontPropertyList.Add(const iItem: TFontProperty);
begin
  if IndexOf(iItem.FRealName) < 0then
    FList.Add(iItem);
end;

procedure TFontPropertyList.Clear;
var
  Prop: TFontProperty;
begin
  for Prop in FList do
    Prop.DisposeOf;

  FList.Clear;
end;

constructor TFontPropertyList.Create;
begin
  inherited Create;

  FList := TList<TFontProperty>.Create;
end;

procedure TFontPropertyList.Delete(const iIndex: Integer);
begin
  if (iIndex > -1) and (iIndex < FList.Count) then
    FList.Delete(iIndex);
end;

destructor TFontPropertyList.Destroy;
begin
  Clear;
  inherited;
end;

function TFontPropertyList.GetCount: Integer;
begin
  Result := FList.Count;
end;

function TFontPropertyList.GetItem(const iIndex: Integer): TFontProperty;
begin
  if (iIndex < 0) or (iIndex >= FList.Count) then
    Result := nil
  else
    Result := FList[iIndex];
end;

function TFontPropertyList.IndexOf(const iRealName: String): Integer;
var
  Name: TFontProperty;
  i: Integer;
begin
  Result := -1;

  for i := 0 to FList.Count - 1 do
  begin
    Name := FList[i];

    if (Name.FRealName = iRealName) then
    begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFontPropertyList.SetItem(
  const iIndex: Integer;
  const Value: TFontProperty);
begin
  if (iIndex > -1) and (iIndex < FList.Count) then
    FList[iIndex] := Value;
end;

procedure TFontPropertyList.Sort;
begin
  FList.Sort(
    TComparer<TFontProperty>.Construct(
      function(const L, R: TFontProperty): Integer
      begin
        Result := CompareText(L.FDisplayName, R.FDisplayName);
      end
    )
  );
end;

end.
