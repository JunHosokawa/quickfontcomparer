﻿(*
 * Font List Getter
 *
 * PLATFORMS
 *   Windows / macOS
 *
 * LICENSE
 *   Copyright (c) 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * HOW TO USE
 *   uses PK.FontList, PK.FontList.Helper;
 *
 *   type
 *     TForm1 = class(TForm)
 *       ListBox1: TListBox;
 *       procedure FormCreate(Sender: TObject);
 *     private
 *     end;
 *
 *   procedure TForm1.FormCreate(Sender: TObject);
 *   begin
 *     LoadFonts(ListBox1, nil, nil);
 *   end;
 *
 * 2018/04/08 Version 1.0.0
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

unit PK.FontList.Helper;

interface

uses
  System.Classes
  , System.SysUtils
  , FMX.ListBox
  , PK.FontList.Default
  ;

type
  TFontPropertyListItem = class(TListBoxItem)
  private var
    FFontProperty: TFontProperty;
  public
    constructor Create(
      const iListBox: TComponent;
      const iFontProperty: TFontProperty); reintroduce;
    function Clone: TFontPropertyListItem;
    property FontProperty: TFontProperty read FFontProperty;
  end;

  TLoadFontsDetermineToAddFunc =
    reference to function(const iFontProperty: TFontProperty): Boolean;

procedure LoadFonts(
  const iListBox: TListBox;
  const iDetermineToAddFunc: TLoadFontsDetermineToAddFunc;
  const iFinishProc: TProc);

implementation

uses
  System.Generics.Collections
  , PK.FontList
  ;

{ TFontPropertyListItem }

function TFontPropertyListItem.Clone: TFontPropertyListItem;
begin
  Result := TFontPropertyListItem.Create(ListBox, FFontProperty);
end;

constructor TFontPropertyListItem.Create(
  const iListBox: TComponent;
  const iFontProperty: TFontProperty);
begin
  inherited Create(iListBox);

  FFontProperty := iFontProperty;
  Text := FFontProperty.DisplayName;
end;

procedure LoadFonts(
  const iListBox: TListBox;
  const iDetermineToAddFunc: TLoadFontsDetermineToAddFunc;
  const iFinishProc: TProc);
const
  STEP = 500;
var
  Count: Integer;
  Items: TList<TFontPropertyListItem>;
begin
  Count := TFontList.Current.Count;

  Items := TList<TFontPropertyListItem>.Create;

  TThread.CreateAnonymousThread(
    procedure
    var
      i: Integer;
    begin
      i := 0;
      while i < Count do
      begin
        TThread.Synchronize(
          TThread.Current,
          procedure
          var
            j: Integer;
            FontProperty: TFontProperty;
          begin
            for j := i to i + STEP - 1 do
            begin
              if j >= Count then
                Break;

              FontProperty := TFontList.Current[j];
              if
                not Assigned(iDetermineToAddFunc) or
                iDetermineToAddFunc(FontProperty)
              then
                Items.Add(TFontPropertyListItem.Create(iListBox, FontProperty));
            end;

            Inc(i, STEP);
          end
        );
      end;

      TThread.Synchronize(
        TThread.Current,
        procedure
        var
          Item: TFontPropertyListItem;
        begin
          TMonitor.Enter(iListBox);
          try
            iListBox.BeginUpdate;
            try
              iListBox.Clear;
              for Item in Items do
                iListBox.Content.AddObject(Item);
            finally
              iListBox.EndUpdate;
            end;

            if Assigned(iFinishProc) then
              iFinishProc;

            if iListBox.Items.Count > 0 then
              iListBox.ItemIndex := 0;

            iListBox.InvalidateContentSize;
          finally
            TMonitor.Exit(iListBox);
          end;
        end
      );
    end
  ).Start;
end;

end.
