﻿(*
 * Font List Getter
 *
 * PLATFORMS
 *   Windows / macOS
 *
 * LICENSE
 *   Copyright (c) 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * HOW TO USE
 *   uses PK.FontList, PK.FontList.Helper;
 *
 *   type
 *     TForm1 = class(TForm)
 *       ListBox1: TListBox;
 *       procedure FormCreate(Sender: TObject);
 *     private
 *     end;
 *
 *   procedure TForm1.FormCreate(Sender: TObject);
 *   begin
 *     LoadFonts(ListBox1, nil, nil);
 *   end;
 *
 * 2018/04/08 Version 1.0.0
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)


unit PK.FontList.Mac;

// (*
{$IFNDEF OSX}
{$WARNINGS OFF 1011}
interface
implementation
end.
{$ENDIF}
// *)

interface

procedure RegisterFontList;

implementation

uses
  System.Classes
  , System.SysUtils
  , FMX.Platform
  , Macapi.Foundation
  , Macapi.AppKit
  , Macapi.Helpers
  , PK.FontList.Default
  ;

type
  TMacFontList = class(TInterfacedObject, IFontList)
  public
    procedure GetFontList(const iList: TFontPropertyList);
  end;

  TMacFontListFactory = class(TFontListFactory)
  public
    function CreateFontList: IFontList; override;
  end;

procedure RegisterFontList;
var
  Factory: TMacFontListFactory;
begin
  Factory := TMacFontListFactory.Create;
  TPlatformServices.Current.AddPlatformService(IFontListFactory, Factory);
end;

{ TMacFontListFactory }

function TMacFontListFactory.CreateFontList: IFontList;
begin
  Result := TMacFontList.Create;
end;

{ TMacFontList }

procedure TMacFontList.GetFontList(const iList: TFontPropertyList);
var
  Manager: NSFontManager;
  FontArray: NSArray;
  Font: NSFont;
  i: Integer;
  LName, RName: NSString;
  RealName, DisplayName: String;
begin
  iList.Clear;

  Manager := TNSFontManager.Wrap(TNSFontManager.OCClass.sharedFontManager);
  FontArray := Manager.availableFontFamilies;
  for i := 0 to FontArray.count - 1 do
  begin
    RName := TNSString.Wrap(FontArray.objectAtIndex(i));
    LName := Manager.localizedNameForFamily(RName, nil);

    RealName := String(UTF8String(RName.UTF8String));
    DisplayName := String(UTF8String(LName.UTF8String));

    Font := TNSFont.Wrap(TNSFont.OCClass.fontWithName(RName, 16));
    if Font = nil then
      Continue;

    iList.Add(
      TFontProperty.Create(
        DisplayName,
        RealName,
        Font.isFixedPitch,
        Font.coveredCharacterSet.characterIsMember($3042)
        // ↑日本語簡易チェック「あ(U+3042)」が含まれているかどうか
      )
    );

    Font := nil;
  end;

  iList.Sort;
end;

initialization
  RegisterFontList;

end.
