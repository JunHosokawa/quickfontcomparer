﻿unit uFavoriteListItem;

interface

uses
  System.Classes
  , System.UITypes
  , System.SysUtils
  , FMX.Objects
  , FMX.Graphics
  , FMX.StdCtrls
  , FMX.ListBox
  , FMX.TextLayout
  , PK.FontList.Default
  , PK.FontList.Helper
  ;

type
  TDetailEvent =
    procedure(
      Sender: TObject;
      const iFontProperty: TFontProperty;
      const iTextSettings: TTextSettings) of object;

  TDeleteEvent =
    procedure(
      Sender: TObject;
      const iFontProperty: TFontProperty) of object;

  TFavoriteListItem = class(TListBoxItem)
  private var
    FFontProperty: TFontProperty;
    FPadSpaces: String;
    FOnAdd: TNotifyEvent;
    FOnDelete: TDeleteEvent;
    FOnCopied: TNotifyEvent;
    FOnDetail: TDetailEvent;
  private
    procedure CheckStatus;
    procedure SetFontNames;
    procedure AddButtonClick(Sender: TObject);
    procedure DeleteButtonClick(Sender: TObject);
    procedure CopyButtonClick(Sender: TObject);
    procedure DetailButtonClick(Sender: TObject);
  protected
    procedure ApplyStyle; override;
  public
    constructor Create(
      const Owner: TComponent;
      const iFontItem: TFontPropertyListItem); reintroduce;
    procedure SetFontPop(
      const iSize: Single;
      const iColor: TAlphaColor;
      const iStyles: TFontStyles);
    procedure SetFontPropertyFromItem(const iFontItem: TFontPropertyListItem);
    property FontProperty: TFontProperty read FFontProperty;
    property PadSpaces: String read FPadSpaces;
    property OnAdd: TNotifyEvent read FOnAdd write FOnAdd;
    property OnDelete: TDeleteEvent read FOnDelete write FOnDelete;
    property OnCopied: TNotifyEvent read FOnCopied write FOnCopied;
    property OnDetail: TDetailEvent read FOnDetail write FOnDetail;
  end;

implementation

uses
  FMX.Platform
  , FMX.Controls
  ;

{ TFontListItem }

procedure TFavoriteListItem.AddButtonClick(Sender: TObject);
begin
  if Assigned(FOnAdd) then
    FOnAdd(Self);
end;

procedure TFavoriteListItem.ApplyStyle;
var
  Button: TButton;
  IsFirst: Boolean;
begin
  inherited;

  IsFirst := Index = 0;

  SetFontNames;
  CheckStatus;

  if FindStyleResource<TButton>('buttonadd', Button) then
  begin
    Button.OnClick := AddButtonClick;
    Button.Visible := IsFirst;
  end;

  if FindStyleResource<TButton>('buttondelete', Button) then
  begin
    Button.OnClick := DeleteButtonClick;
    Button.Visible := not IsFirst;
  end;

  if FindStyleResource<TButton>('buttoncopy', Button) then
    Button.OnClick := CopyButtonClick;

  if FindStyleResource<TButton>('buttondetail', Button) then
    Button.OnClick := DetailButtonClick;
end;

procedure TFavoriteListItem.CheckStatus;
var
  Mono: TText;
  Japanese: TImage;
begin
  if FFontProperty <> nil then
  begin
    if FindStyleResource<TText>('textMonospace', Mono) then
      Mono.Opacity := Ord(FFontProperty.IsMonoSpace);

    if FindStyleResource<TImage>('imageJapanese', Japanese) then
      Japanese.Opacity := Ord(FFontProperty.IsJapanese);
  end;
end;

procedure TFavoriteListItem.CopyButtonClick(Sender: TObject);
var
  Clipboard: IFMXClipboardService;
begin
  if
    (FFontProperty <> nil) and
    TPlatformServices.Current.SupportsPlatformService(
      IFMXClipboardService,
      Clipboard
    )
  then
  begin
    Clipboard.SetClipboard(FFontProperty.RealName);

    if Assigned(FOnCopied) then
      FOnCopied(Self);
  end;
end;

constructor TFavoriteListItem.Create(
  const Owner: TComponent;
  const iFontItem: TFontPropertyListItem);
begin
  inherited Create(Owner);

  StyledSettings := [];
  StyleLookup := 'fontlistitem';

  CanFocus := False;
  TextSettings;

  FFontProperty := nil;

  if iFontItem <> nil then
    SetFontPropertyFromItem(iFontItem);
end;

procedure TFavoriteListItem.DeleteButtonClick(Sender: TObject);
begin
  ListBox.Content.RemoveObject(Self);
  if (FFontProperty <> nil) and Assigned(FOnDelete) then
    FOnDelete(Self, FFontProperty);
end;

procedure TFavoriteListItem.DetailButtonClick(Sender: TObject);
begin
  if Assigned(FOnDetail) then
    FOnDetail(Self, FFontProperty, TextSettings);
end;

procedure TFavoriteListItem.SetFontNames;
var
  zW, fW, SW: Single;
  FontName: TText;
  UpperFontName: String;

  function CalcBy: Single;
  type
    TSpecialFont = record
      FontName: String;
      By: Single;
    end;
  const
    SPECIAL_FONTS: array [0.. 3] of TSpecialFont = (
      (FontName:'ITC';         By: 1.3),
      (FontName:'ITALIC';      By: 1.2),
      (FontName:'SCRIPT';      By: 1.5),
      (FontName:'BRAVURATEXT'; By: 1.7)
    );
  var
    SF: TSpecialFont;
  begin
    Result := 1;

    for SF in SPECIAL_FONTS do
      if UpperFontName.IndexOf(SF.FontName) > -1 then
      begin
        Result := SF.By;
        Break;
      end;
  end;

begin
  if FFontProperty = nil then
    Exit;

  if FindStyleResource<TText>('fontname', FontName) then
    FontName.Text := FFontProperty.DisplayName;

  TextSettings.Font.Family := FFontProperty.RealName;

  FPadSpaces := '';
  if (Canvas <> nil) and (Canvas.Font <> nil) then
  begin
    Canvas.Font.Family := FFontProperty.RealName;
    UpperFontName := FFontProperty.RealName.ToUpper;

    if (not FFontProperty.IsMonoSpace) and (not FFontProperty.IsJapanese) then
    begin
      zW := Canvas.TextWidth('z');
      fW := Canvas.TextWidth('f') * CalcBy;
      SW := Canvas.TextWidth(' ');

      if SW > 0 then
        while fW > zW do
        begin
          FPadSpaces := FPadSpaces + ' ';
          zW := zW + SW;
        end;
    end;
  end;
end;

procedure TFavoriteListItem.SetFontPop(
  const iSize: Single;
  const iColor: TAlphaColor;
  const iStyles: TFontStyles);
begin
  TextSettings.Font.Size := iSize;
  TextSettings.Font.Style := iStyles;
  TextSettings.FontColor := iColor;
end;

procedure TFavoriteListItem.SetFontPropertyFromItem(
  const iFontItem: TFontPropertyListItem);
begin
  if iFontItem = nil then
    FFontProperty := nil
  else
    FFontProperty := iFontItem.FontProperty;

  CheckStatus;
  SetFontNames;
end;

end.
