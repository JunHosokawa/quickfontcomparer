﻿unit uColorDialog;

interface

uses
  System.UITypes
  , FMX.Objects
  , FMX.Edit
  , FMX.StdCtrls
  , FMX.Colors
  , PK.Utils.RectDialog
  ;

type
  TColorPickEvent =
    procedure(Sender: TObject; const iColor: TAlphaColor) of object;

  TColorDialog = class(TRectDialog)
  private var
    FOnPick: TColorPickEvent;
    FEdit: TEdit;
    FColorPanel: TColorPanel;
    FButton: TColorButton;
  private
    procedure EditValidating(Sender: TObject; var Text: string);
    procedure ColorPanelChange(Sender: TObject);
    procedure ColorToText;
  public
    constructor Create(
      const iBase, iDisabler: TRectangle;
      const iColorPanel: TColorPanel;
      const iEdit: TEdit;
      const iCloseButton: TCustomButton); reintroduce;
    procedure Show(const iButton: TColorButton); reintroduce;
    property Sender: TColorButton read FButton;
    property OnPick: TColorPickEvent read FOnPick write FOnPick;
  end;

implementation

uses
  System.Classes
  , System.SysUtils
  , FMX.Types
  , FMX.Controls
  ;

{ TColorDialog }

procedure TColorDialog.ColorPanelChange(Sender: TObject);
begin
  ColorToText;

  if Assigned(FOnPick) then
    FOnPick(Self, FColorPanel.Color);
end;

procedure TColorDialog.ColorToText;
begin
  FEdit.Text :=Integer(FColorPanel.Color).ToHexString(8);
end;

constructor TColorDialog.Create(
  const iBase, iDisabler: TRectangle;
  const iColorPanel: TColorPanel;
  const iEdit: TEdit;
  const iCloseButton: TCustomButton);
begin
  inherited Create(iBase, iDisabler, iCloseButton);

  FEdit := iEdit;
  FColorPanel := iColorPanel;

  FEdit.OnValidating := EditValidating;
  FColorPanel.OnChange := ColorPanelChange;

  ColorToText;
end;

procedure TColorDialog.EditValidating(Sender: TObject; var Text: string);
var
  Valid: String;
  C: Char;
begin
  Valid := '';
  for C in Text do
  begin
    if CharInSet(C, ['0'.. '9', 'a'.. 'f', 'A'.. 'F']) then
      Valid := Valid + C;

    if Valid.Length > 7 then
      Break;
  end;

  Text := Valid;

  FColorPanel.Color := StrToIntDef('$' + Valid, FColorPanel.Color);
end;

procedure TColorDialog.Show(const iButton: TColorButton);
var
  X: Single;
  Control: TControl;
  P: TFmxObject;
begin
  FButton := iButton;

  X := 0;
  Control := FButton;
  while Control <> nil do
  begin
    X := X + Control.Position.X;
    P := Control.Parent ;
    if (P is TControl) then
      Control := TControl(P)
    else
      Break;
  end;

  Base.Position.X := X;
  FColorPanel.Color := iButton.Color;

  inherited Show;
end;

end.
