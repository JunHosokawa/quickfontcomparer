﻿unit uDetailDialog;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  PK.FontList.Default, FMX.ScrollBox, FMX.Memo, FMX.Objects,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts, PK.LimitSize, FMX.Edit;

type
  TfrmDetailDialog = class(TForm)
    layoutBottom: TLayout;
    btnDetailClose: TButton;
    memoDetail: TMemo;
    SizeGrip1: TSizeGrip;
    Layout1: TLayout;
    lblFontName: TLabel;
    edtInputUnicode: TEdit;
    Label1: TLabel;
    btnInputUnicode: TButton;
    Path1: TPath;
    procedure memoDetailApplyStyleLookup(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnDetailCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnInputUnicodeClick(Sender: TObject);
    procedure edtInputUnicodeKeyDown(Sender: TObject; var Key: Word;
      var KeyChar: Char; Shift: TShiftState);
    procedure FormDestroy(Sender: TObject);
  private var
    FLimitSize: TLimitSize;
    FBackColor: TAlphaColor;
  public
    class procedure New(
      const iOwner: TCommonCustomForm;
      const iText: String;
      const iFontProperty: TFontProperty;
      const iTextSettings: TTextSettings;
      const iBackColor: TAlphaColor);
  end;

implementation

{$R *.fmx}

{ TfrmDetailDialog }

procedure TfrmDetailDialog.btnDetailCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmDetailDialog.btnInputUnicodeClick(Sender: TObject);
var
  Code: Integer;
  Text: String;
  SelStart: Integer;
begin
  Code := StrToIntDef('$' + edtInputUnicode.Text, -1);
  if Code < 0 then
    Exit;

  SelStart := memoDetail.SelStart;
  Text := memoDetail.Text;

  memoDetail.Text :=
    Copy(Text, 1, SelStart) + Chr(Code) + Copy(Text, SelStart + 1);

  memoDetail.SelStart := SelStart + 1;
end;

procedure TfrmDetailDialog.edtInputUnicodeKeyDown(
  Sender: TObject;
  var Key: Word;
  var KeyChar: Char;
  Shift: TShiftState);
begin
  case Key of
    0:
    begin
      if (not CharInSet(KeyChar, ['0'.. '9', 'a'.. 'f', 'A'.. 'F'])) then
        KeyChar := #0;
    end;

    vkReturn:
    begin
      btnInputUnicodeClick(Self);
      Key := 0;
    end;
  end;
end;

procedure TfrmDetailDialog.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := TCloseAction.caFree;
end;

procedure TfrmDetailDialog.FormCreate(Sender: TObject);
begin
  FLimitSize := TLimitSIze.Create(Self);
  FLimitSize.Parent := Self;
  FLimitSize.SetLimit(480, 240, 0, 0);
end;

procedure TfrmDetailDialog.FormDestroy(Sender: TObject);
begin
  FLimitSize.DisposeOf;
end;

procedure TfrmDetailDialog.memoDetailApplyStyleLookup(Sender: TObject);
var
  R: TRectangle;
begin
  if memoDetail.FindStyleResource<TRectangle>('background', R) then
    R.Fill.Color := FBackColor;
end;

class procedure TfrmDetailDialog.New(
  const iOwner: TCommonCustomForm;
  const iText: String;
  const iFontProperty: TFontProperty;
  const iTextSettings: TTextSettings;
  const iBackColor: TAlphaColor);
begin
  with TfrmDetailDialog.Create(iOwner) do
  begin
    FBackColor := iBackColor;

    StyleBook := iOwner.StyleBook;

    if iFontProperty = nil then
      lblFontName.Text := ''
    else
      lblFontName.Text := iFontProperty.ToString;

    memoDetail.TextSettings.Assign(iTextSettings);
    memoDetail.Text := iText;
    memoDetail.SelStart := iText.Length;
    memoDetail.WordWrap := True;
    memoDetail.SetFocus;

    Show;
  end;
end;

end.
