# Quick Font Comparer

## Overview
This software is a tool for comparing fonts

## Environment
Delphi 10.3 Rio Release 3  
FireMonkey

## Support OS
Windows 10, macOS Catalina

## ScreenShot
![ScreenShot](https://bitbucket.org/JunHosokawa/quickfontcomparer/downloads/qfcss.png)

## Contact
freeonterminate@gmail.com  
http://twitter.com/pik  

# LICENSE
Copyright (c) 2018, 2020 HOSOKAWA Jun  
Released under the MIT license  
http://opensource.org/licenses/mit-license.php
